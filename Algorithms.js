/**
 * Created by Dima on 24.06.2016.
 */
/*
* for discussion:
*  http://practical-programming.blogspot.com/2011/12/blog-post.html
*/

/*
 * get data about array of numbers
 * maxArrayLength = Math.pow(2, 32) - 1 for analyze
 *
 * @numArr int[]
 * */
(function(numArr) {
	var abs = function (int) {
		return int >= 0 ? int : ~(int) + 1;//reversing bites
	};

	/*
	* get min and max values
	*
	* @numArr int[]
	*
	* return {
	*   max: int
	*   min: int
	* }
	* */
	var range = (function (numArr) {
		var min = Infinity;
		var max = -Infinity;

		for (var i = 0; i < numArr.length; i += 1) {
			if (min > numArr[i]) {
				min = numArr[i];
			}

			if (max < numArr[i]) {
				max = numArr[i];
			}
		}

		return {
			min: min,
			max: max
		}
	})(numArr);


	/*
	* initialize counter for checking missing and repeating numbers
	*
	* @numArr int[]
	* @range {
	*   max: int
	*   min: int
	* }
	*
	* return int[]
	* */
	var counter = (function (numArr, range) {
		var cnt = new Array(abs(range.min) + abs(range.max) + 1);

		for (var i = 0; i < cnt.length; i += 1) {
			cnt[i] = 0;
		}

		for (var i = 0; i < numArr.length; i += 1) {
			cnt[abs(range.min) + numArr[i]] += 1;
		}

		return cnt;
	})(numArr, range);


	/*
	* analyze and get repeating and missing numbers from counter for printing
	*
	* @counter int[]
	* @range {
	*   max: int
	*   min: int
	* }
	*
	* return {
	*   repeatingNumbers: {},
	*   missingNumbers: int[],
	* }
	* */
	var data = (function (counter, range) {
		var repeatingNumbers = {};
		var missingNumbers = [];

		for (var i = 0; i < counter.length; i += 1) {
			var num = i - abs(range.min);

			if (counter[i] > 1) {
				repeatingNumbers[num] = counter[i];
			} else if (counter[i] === 0) {
				missingNumbers.push(num);
			}
		}

		return {
			repeatingNumbers: repeatingNumbers,
			missingNumbers: missingNumbers
		}
	})(counter, range);

	return {
		min: range.min,
		max: range.max,
		repeatingNumbers: data.repeatingNumbers,
		missingNumbers: data.missingNumbers
	}
})([3, 1, -5, 3, 3, 5, 0, 1, 1, 3]);
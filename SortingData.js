/*
 * module 'comparator'
 * @param string || function
 * @opt {
 *     direction: int, (defulat 1 || -1)
 *     ignoreCase: boolean (defulat false)
 * }
 */
var comparator = (function() {
	var makeCompareFunction = function(param, opt) {
		var preparation = function(value) {
			return (opt.ignoreCase && typeof value === 'string') ? value.toLowerCase() : value;
		};
		var compareFunction, compareUnaryFunction, prop;

		if (typeof param !== 'function') {
			//maybe any value
			prop = param;

			//make unary function
			compareFunction = function(a) {
				return !!a[prop] ? a[prop] : '';
			}
		} else {
			compareFunction = param;
		}

		if (typeof compareFunction === 'function') {
			if (compareFunction.length === 1) {
				//compareFunction is a unary function mapping a single item to its sort score
				compareUnaryFunction = compareFunction;

				compareFunction = function(a, b) {
					var score = 0;

					a = preparation(compareUnaryFunction(a));

					b = preparation(compareUnaryFunction(b));

					if (a < b) {
						score = -1;
					} else if (a > b) {
						score = 1;
					}

					return score;
				}
			}
		}

		//return reverse of or normal sorting
		return opt.direction !== -1 ? compareFunction : function(a, b) {
			return -1 * (compareFunction(a, b) || 0);
		};
	};

	return function next (param, opt) {
		opt = opt || {
				direction: 1,
				ignoreCase: false
			};
		var context = typeof this === 'function' ? this : false;
		//initialize compare functions for using
		var nextCompareFunction = makeCompareFunction(param, opt);
		var currentCompareFunction = (function(context, nextCompareFunction) {
			var compare = function(a, b) {
				return context(a, b) || nextCompareFunction(a, b);
			};

			return context ? compare : nextCompareFunction;
		})(context, nextCompareFunction);
		//---------------------------------

		currentCompareFunction.next = next;

		return currentCompareFunction;
	}
})();


var data = [
	['user_id', 'module', 'points', 'velocity_index', 'strength_index', 'module_rank'],
	[100, 'module 2', 123, 0.89, 0.96, 35],
	[100, 'module 5', 87, 0.96, 0.54, 64],
	[150, 'module 2', 234, 0.75, 0.87, 43],
	[150, 'module 3', 76, 0.98, 0.97, 78]
];


var preparedData = (function(data) {
	var cloneData = JSON.parse(JSON.stringify(data));
	var names = cloneData.shift();

	return cloneData.map(function(arrData) {
		var row = {};

		arrData.forEach(function(val, j) {
			row[names[j]] = val;
		});

		return row;
	});
});


//examples
console.table(
	preparedData(data).sort(
		comparator('module_rank')
	)
);

console.table(
	preparedData(data).sort(
		comparator(function (a, b) {
			return a.points > b.poins;
		})
	)
);

console.table(
	preparedData(data).sort(
		comparator('module_rank').next(function (a, b) {
			return a.points - b.poins;
		}).next(function (a, b) {
			return a.velocity_index - b.velocity_index;
		})
	)
);
//-------------------------